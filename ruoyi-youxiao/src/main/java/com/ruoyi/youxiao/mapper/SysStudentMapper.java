package com.ruoyi.youxiao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.youxiao.domain.SysStudent;

/**
 * 学生信息Mapper接口
 *
 * @author orange
 */
public interface SysStudentMapper extends BaseMapper<SysStudent> {

}

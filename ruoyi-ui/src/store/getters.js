const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  // 设备信息：PC还是手机
  device: state => state.app.device,
  // 数据字典
  dict: state => state.dict.dict,
  // 已访问记录
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  // 令牌
  token: state => state.user.token,
  // 用户头像
  avatar: state => state.user.avatar,
  // 用户姓名
  name: state => state.user.name,
  // 用户简介
  introduction: state => state.user.introduction,
  // 所有角色
  roles: state => state.user.roles,
  // 所有权限
  permissions: state => state.user.permissions,
  // 权限路由
  permission_routes: state => state.permission.routes,
  // 一级菜单在头部显示出来的
  topbarRouters:state => state.permission.topbarRouters,
  // 默认路由
  defaultRoutes:state => state.permission.defaultRoutes,
  // 侧边栏路由
  sidebarRouters:state => state.permission.sidebarRouters,
}
export default getters
